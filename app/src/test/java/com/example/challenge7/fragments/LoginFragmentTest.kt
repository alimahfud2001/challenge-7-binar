package com.example.challenge7.fragments

import com.google.common.truth.Truth
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test

class LoginFragmentTest : TestCase() {

    @Test
    fun testSetTrue() {
        val gson = Gson()
        val dataJson = arrayListOf("username", "email", "password")
        val data = gson.toJson(dataJson)
        val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)

        Truth.assertThat(set.elementAt(0) == "username").isTrue()
    }

    @Test
    fun testSetFalse() {
        val gson = Gson()
        val dataJson = arrayListOf("username", "email", "password")
        val data = gson.toJson(dataJson)
        val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)

        Truth.assertThat(set.elementAt(0) == "email").isFalse()
    }
}