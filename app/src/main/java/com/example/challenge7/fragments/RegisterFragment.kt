package com.example.challenge7.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.challenge7.databinding.FragmentRegisterBinding
import com.example.challenge7.manager.DataStoreManager
import com.example.challenge7.viewmodel.MovieViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.koin.android.ext.android.inject


class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    private val viewmodel: MovieViewModel by inject()
    private val pref: DataStoreManager by inject()
    private var data = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref.setContext(requireContext())

        binding.et2.setOnFocusChangeListener({ v, hasFocus -> if (!hasFocus) observer(binding.et2.text.toString()) })

        binding.btnDaftar.setOnClickListener {
            val username = binding.ti1.editText?.text.toString()
            val email = binding.ti2.editText?.text.toString()
            val password = binding.ti3.editText?.text.toString()
            val confirmPassword = binding.ti4.editText?.text.toString()
            val gson = Gson()

            if (username.isNullOrEmpty()||email.isNullOrEmpty()||password.isNullOrEmpty()||confirmPassword.isNullOrEmpty())
                Toast.makeText(context, "Isi Detail", Toast.LENGTH_SHORT).show()
            else {
                val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)
                if (set?.elementAt(1) != null) {
                    val dialog = AlertDialog.Builder(requireContext())
                    dialog.setTitle("Pendaftaran Gagal")
                    dialog.setMessage("Email sudah terdaftar")
                    dialog.setPositiveButton("Ok"){ dialogInterface, p1 -> }
                    dialog.show()
                }
                else {
                    if (password != confirmPassword) {
                        Toast.makeText(context, "Password tidak cocok", Toast.LENGTH_SHORT).show()
                    }
                    else {
                        val _set = arrayListOf(username, email, password)
                        viewmodel.saveDataStore(email, gson.toJson(_set))
                        val dialog = AlertDialog.Builder(requireContext())
                        dialog.setTitle("Pendaftaran Berhasil")
                        dialog.setMessage("Silahkan Login")
                        dialog.setPositiveButton("Login"){ dialogInterface, p1 ->
                            view.findNavController().popBackStack()
                        }
                        dialog.show()
                    }
                }
            }
        }
    }

    private fun observer(email:String) {
        viewmodel.apply {
            getDataStore(email).observe(requireActivity()) {
                data = it
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}