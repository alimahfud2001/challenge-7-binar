package com.example.challenge7.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.challenge7.R
import com.example.challenge7.adapter.MovieAdapter
import com.example.challenge7.databinding.FragmentHomeBinding
import com.example.challenge7.manager.DataStoreManager
import com.example.challenge7.model.GetMovieResponseItem
import com.example.challenge7.viewmodel.MovieViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.*
import org.koin.android.ext.android.inject
import java.io.IOException


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding
    private val viewmodel : MovieViewModel by inject()
    private val pref: DataStoreManager by inject()
    private val client: OkHttpClient by inject()
    private var email = ""
    private var data = ""
    private val gson = Gson()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
         }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref.setContext(requireContext())
        observer()

        viewmodel.movieList.observe(requireActivity(), Observer {
            Log.d("listMovie", "$it")
            if (viewmodel.code == 200){
                showList(it)
                binding?.progressBar?.visibility = View.GONE
            }
            else binding?.progressBar?.visibility = View.GONE
        })
        viewmodel.fetchAllData()

        binding?.ivHomeToprofil?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }

        chucker()
    }

    fun chucker() {
        val request  = Request.Builder().url("https://api.themoviedb.org/3/movie/top_rated/e587910f9f7819198c62e384c9fdfaaf")
            .build()

        CoroutineScope(Dispatchers.IO).launch {
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {
                        val myResponse: String? = response.body?.string()
                        Log.d("movieresponse", myResponse.toString())
                    }
                }
            })
        }
    }

    private fun observer() {
        viewmodel.apply {
            getLoginStatus().observe(requireActivity()){
                email = it
                viewmodel.getDataStore(email).observe(requireActivity()){
                    data = it
                    val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)
                    if (set!=null) binding?.tvHomeHello?.setText("Hi, ${set.elementAt(0)} ")
                }
            }
        }
    }

    private fun showList(data: List<GetMovieResponseItem>?){
        val adapter = MovieAdapter(requireContext())
        adapter.submitData(data)
        binding?.recyclerview?.setLayoutManager(GridLayoutManager(requireContext(), 3))
        binding?.recyclerview?.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}